# ESP32 CAN Shield project:
This project is a contribution to the open-source community or to anyone has interest to exploit the connectivity possibilities offered by the marvelous ESP32, interfaced with the **MCP2515** CAN controller in a fashionable way:

![alt text](03_MISC/ESP32_CAN_Shield.png)

![alt text](03_MISC/ESP32_CAN_Shield_2.png)

This project is divided into two major parts, the hardware part under **02_HW** folder which contains the schematic and the PCB design for the CAN shield (this is just an initial HW implementation further enhancements and other HW variants will be added later) using **KiCad EDA**, and the software part under **01_SW** folder which contains a lightweight and flexible driver implementation for the **MCP2515** (please check 01_SW/README.md for how to use the MCP2515 driver).
The **03_MISC** folder contains the final result pictures and photos.

I hope this project will be useful, if you have any questions, comments or ideas please don’t hesitate to write down a comment :)
